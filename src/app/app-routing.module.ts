import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';
import {AuthGuardService as AuthGuard} from './auth-guard.service';

const routes: Routes = [
  {path: '', component: HomeComponent,
    children: [
      {path: '', component: LoginComponent},
      { path : 'register', component: RegisterComponent }
    ] 
  },
  {path: 'dashboard', component:DashboardHomeComponent, canActivate: [AuthGuard], 
  children: [
    {path: '', component: DashboardComponent},
    { path : 'register', component: RegisterComponent }
    ] 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
