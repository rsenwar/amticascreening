import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class APIService {

  constructor(private http:Http) { }

  login(model:any) {
    return this.http.get('http://13.232.145.11:8010/login?email='+model.email+'&password='+model.password);
  }

  register(model:any) {
    delete model.cpassword;
    return this.http.post('http://13.232.145.11:8010/register', model);
  }
}
