import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  data:any={};
  submitted:boolean;
  registered:boolean;
  constructor(private APIService:APIService,private router: Router,) { }

  ngOnInit() {
  }
  register() {
    this.registered = false;
    this.submitted = false;
    this.APIService.register(this.data).subscribe(
      data => {
        let d = data.json();
        if(d.success) {
          this.submitted = true;
          this.router.navigate(['']);
        } else {
          this.registered = true;
        }
      }
    )
    console.log(this.data);
  }
}
