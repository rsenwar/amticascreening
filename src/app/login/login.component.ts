import { Component, OnInit } from '@angular/core';
import {APIService} from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data:any={};
  inv:boolean;
  constructor(private APIService:APIService, public router:Router) { }


  ngOnInit() {
  }
  login() {
    this.inv = false;
    this.APIService.login(this.data).subscribe(
      data => {
        let d = data.json();
        if(d.success) {
          localStorage.setItem('user', JSON.stringify(d));
          this.router.navigate(['/dashboard']);
        } else {
          this.inv = true;
          this.data = {};
        }
      }
    )
    console.log(this.data.email,this.data.password);
  }
}
